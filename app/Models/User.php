<?php

namespace App\Models;

class User
{
    protected $firstName;
    
    protected $lastName;

    protected $email;

    public function setFirstName(String $firstName = null)
    {
        $this->firstName = str_replace(' ', '', $firstName);
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setLastName(String $lastName = null)
    {
        $this->lastName = str_replace(' ', '', $lastName);
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getUserEmail()
    {
        return $this->email;
    }

    public function setUserEmail($email)
    {
        $this->email = $email;
    }

    public function getEmailVariables()
    {
        return array(
            'full_name' => $this->getFullName(),
            'email' => $this->getUserEmail()
        );
    }
}