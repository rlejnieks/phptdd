<?php

class CollectionTest extends \PHPUnit\Framework\TestCase
{
    protected $collection;

    // phpUnit method. Will be called before each test
    protected function setUp()
    {
        $this->collection = new \Core\Collection();
    }

    /** @test */
    public function empty_instantiated_collection_returns_no_items()
    {
        $this->assertEmpty($this->collection->get());
    }

    /** @test */
    public function count_is_correct_for_items_passed_in()
    {
        $this->collection->set(array(
            'one', 'two', 'three'
        ));

        $this->assertEquals(3, $this->collection->count());

    }

    /** @test */
    public function items_returned_match_items_passed_in()
    {
        $this->collection->set(array(
            'one', 'two'
        ));

        $this->assertCount(2, $this->collection->get());
        $this->assertEquals($this->collection->get()[0], 'one');
        $this->assertEquals($this->collection->get()[1], 'two');
    }

    /** @test */
    public function is_collection_an_instance_of_iterator_aggregate()
    {
        
        $this->assertInstanceOf(IteratorAggregate::class, $this->collection);       
    }

    /** @test */
    public function collection_can_be_iterated()
    {
        $this->collection->set(array(
            'one', 'two', 'three'
        ));
        
        $items = array();

        foreach($this->collection as $item){
            $items[] = $item;
        }

        $this->assertCount(3, $items);
        $this->assertInstanceOf(ArrayIterator::class, $this->collection->getIterator());
    
    }

    /** @test */
    public function collection_can_be_merged_with_another_collection()
    {
        $collection1 = new \Core\Collection(['one', 'two']);
        $collection2 = new \Core\Collection(['three', 'four', 'five']);

        $collection1->merge($collection2);

        $this->assertCount(5, $collection1->get());
        $this->assertEquals(5, $collection1->count());
    }

    /** @test */
    public function new_items_can_be_added_to_old_ones()
    {
        $this->collection->set(array(
            'one', 'two', 'three'
        ));

        $this->collection->add(array(
            'one', 'two', 'three'
        ));

        $this->assertCount(6, $this->collection->get());
    }

    /** @test */
    public function returns_json_encoded_items()
    {
        $this->collection->set(array(
            ['username' => 'Reinis'],
            ['username' => 'Kaspars']
        ));

        $this->assertInternalType('string',  $this->collection->toJson());
        $this->assertEquals('[{"username":"Reinis"},{"username":"Kaspars"}]',  $this->collection->toJson());
    }

    /** @test */
    public function json_encoding_a_collection_object_returns_json()
    {
        $this->collection->set(array(
            ['username' => 'Reinis'],
            ['username' => 'Kaspars']
        ));

        $encoded = json_encode($this->collection);

        $this->assertInternalType('string',  $encoded);
        $this->assertEquals('[{"username":"Reinis"},{"username":"Kaspars"}]',  $encoded);
    }
}