<?php

class UserTest extends \PHPUnit\Framework\TestCase
{
    
    // phpUnit method. Will be called before each test
    protected function setUp()
    {
        $this->user = new \App\Models\User();
    }
    
    /** @test */
    public function can_we_get_the_first_name()
    {

        $this->user->setFirstName('Reinis');

        $this->assertEquals($this->user->getFirstName(), 'Reinis');
    }
    
    /** @test */
    public function can_we_get_the_last_name()
    {

        $this->user->setLastName('Lejnieks');

        $this->assertEquals($this->user->getLastName(), 'Lejnieks');
    }

    /** @test */
    public function can_we_get_the_full_name()
    {

        $this->user->setFirstName('Reinis');
        $this->user->setLastName('Lejnieks');

        $this->assertEquals($this->user->getFullName(), 'Reinis Lejnieks');
    }

    /** @test */
    public function are_first_and_last_name_trimmed()
    {

        $this->user->setFirstName('    Reinis');
        $this->user->setLastName('Lejnieks    ');

        $this->assertEquals($this->user->getFirstName(), 'Reinis');
        $this->assertEquals($this->user->getLastName(), 'Lejnieks');
    }

    /** @test */
    public function can_email_address_be_set()
    {

        $this->user->setUserEmail('rlejnieks@gmail.com');

        $this->assertEquals($this->user->getUserEmail(), 'rlejnieks@gmail.com');
    }

    /** @test */
    public function do_email_variables_contain_correct_values()
    {

        $this->user->setFirstName('    Reinis');
        $this->user->setLastName('Lejnieks    ');
        $this->user->setUserEmail('rlejnieks@gmail.com');

        $emailVariables = $this->user->getEmailVariables();

        // this tests that the returned array from db has these keys and we can use them further
        // full_name not fullName because of potential MySQL col names
        $this->assertArrayHasKey('full_name', $emailVariables);
        $this->assertArrayHasKey('email', $emailVariables);

        $this->assertEquals($emailVariables['full_name'], 'Reinis Lejnieks');
        $this->assertEquals($emailVariables['email'], 'rlejnieks@gmail.com');
    }
}