<?php

namespace Core;

use IteratorAggregate;
use ArrayIterator;
use JsonSerializable;

class Collection implements IteratorAggregate, JsonSerializable
{
    protected $items = array();

    public function __construct(Array $items = array())
    {
        $this->items = $items;
    }

    public function get()
    {
        return $this->items;
    }

    public function set(Array $items = null)
    {
        $this->items = $items;
    }

    public function count()
    {
        return count($this->items);
    }

    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    public function add(Array $items)
    {
        $this->items = array_merge($this->items, $items);
    }

    public function merge(Collection $collection)
    {
        // return new Collection(array_merge($this->get(), $collection->get()));
        return $this->add($collection->get());
    }

    public function toJson()
    {
        return json_encode($this->get());
    }

    public function jsonSerialize()
    {
        return $this->items;
    }
}